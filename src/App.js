/**
 * App
 *
 * Main application window.
 */

import React from 'react';
import { inject, observer } from 'mobx-react';


@inject('store')
@observer
class App extends React.Component {
    render() {
        return (
            <div className="App">
            </div>
        );
    }
}


export default App;
