/**
 * Root Store
 *
 * Contains main branches of state.
 */

import UIStore from './ui';
import DataStore from './data';


export default class RootStore {

    constructor() {
        this.ui = new UIStore(this);
        this.data = new DataStore(this);
    }

}
