/**
 * Data Store
 *
 * Contains the state of the data that is used throughout the site.
 */


export default class DataStore {

    constructor(root) {
        this.root = root;
    }

};
